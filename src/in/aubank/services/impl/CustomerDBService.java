package in.aubank.services.impl;

import java.util.List;
import java.util.Objects;

import in.aubank.model.Customer;
import in.aubank.repository.CustomerRepository;
import in.aubank.repository.impl.CustomerDBRepository;
import in.aubank.services.CustomerService;

public class CustomerDBService implements CustomerService{
	
	CustomerRepository repository=new CustomerDBRepository();

	@Override
	public String addCustomer(Customer customer) {
		if(Objects.isNull(customer)) {
			return "Customer Data is blank";
		}
		else {
			
			String result=repository.addCustomer(customer);
			return result;
		}
	}

	@Override
	public List<Customer> getAllCustomers() {
		
		return repository.getAllCustomers();
	}
	
	

}
