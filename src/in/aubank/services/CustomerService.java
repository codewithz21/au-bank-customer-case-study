package in.aubank.services;

import java.util.List;

import in.aubank.model.Customer;

public interface CustomerService {
	
	public String addCustomer(Customer customer);
	public List<Customer> getAllCustomers();

}
