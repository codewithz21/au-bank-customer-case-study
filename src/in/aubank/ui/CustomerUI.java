package in.aubank.ui;

import java.util.Scanner;

import in.aubank.controller.CustomerContoller;
import in.aubank.services.CustomerService;
import in.aubank.services.impl.CustomerDBService;
import in.aubank.services.impl.CustomerListService;

public class CustomerUI {
	
	public static void start() {
		
		CustomerContoller controller=null;
		CustomerService service=null;
		
		
		Scanner scanner=new Scanner(System.in);
		while(true) {
			System.out.print("Select your reposoitory [List,DB] :");
			String repoOption=scanner.next();
			if(repoOption.equalsIgnoreCase("list")||repoOption.equalsIgnoreCase("db")) {
				if(repoOption.equalsIgnoreCase("list")) {
					service=new CustomerListService();
				}
				if(repoOption.equalsIgnoreCase("db")) {
					service=new CustomerDBService();
				}
				break;
			}
			else {
				System.out.println("Please select between List and DB");
			}
		}
		
		controller=new CustomerContoller(service);
		
		while(true) {
			
			System.out.println("Please enter your choice");
			System.out.println("1 for Adding a new customer");
			System.out.println("2 for displaying all the customers");
			System.out.println("3 for Searching a customer");
			System.out.println("4  for Deleting  a customer");
			System.out.println("5  for exiting the application");
			
			int option=scanner.nextInt();
			
			switch (option) {
			case 1:
				controller.add();
				break;
			case 2:
				controller.showAllCustomers();
				break;
			case 3:
				System.out.println("Searching a  customers");
				break;
			case 4:
				System.out.println("Deleting  a  customers");
				break;
			case 5:
				System.out.println("--------- THnk you for using AU Bank Service-------");
				System.exit(0);
			default:
				System.out.println("Please enter a valid option");
				break;
			}
			
		}
		
		
	}
	
	

}
