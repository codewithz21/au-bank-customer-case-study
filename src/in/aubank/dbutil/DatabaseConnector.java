package in.aubank.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseConnector {
	
	String url="jdbc:postgresql://localhost:5432/au-bank-sept-2022";
	String username="postgres";
	String password="admin";
	
	Connection con=null;
	
	
	private Connection getConnection() {
		
		try {
			Class.forName("org.postgresql.Driver");
			con=DriverManager.getConnection(url,username,password);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return con;
		
	}
	
	public PreparedStatement getPreparedStatement(String query) {
		PreparedStatement pstmt=null;
		try {
			
			con=getConnection();
			pstmt=con.prepareStatement(query);
			
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return pstmt;
	}
	
	public void closeConnection() {
		if(con!=null) {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
