package in.aubank.repository.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import in.aubank.dbutil.DatabaseConnector;
import in.aubank.model.Customer;
import in.aubank.repository.CustomerRepository;

public class CustomerDBRepository implements CustomerRepository{
	
	DatabaseConnector db=new DatabaseConnector();

	@Override
	public String addCustomer(Customer customer) {
		
		String result="";
		try {
			String query="Insert into customer(name,email,phone,date_of_creation) "
					+ "values (?,?,?,?)";
			PreparedStatement pstmt=db.getPreparedStatement(query);
			pstmt.setString(1, customer.getName());
			pstmt.setString(2, customer.getEmail());
			pstmt.setString(3,customer.getPhoneNumber());
			pstmt.setObject(4, customer.getAccountCreationDate());
			
			int rowsInserted=pstmt.executeUpdate();
			
			result=rowsInserted==1?"SUCCESS":"FAILURE";
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return result;
		
	}

	@Override
	public List<Customer> getAllCustomers() {
		List<Customer> customers=new ArrayList<Customer>();
		
		try {
			String query="Select id,name,email,phone,date_of_creation from customer";
			PreparedStatement pstmt=db.getPreparedStatement(query);
			ResultSet rs=pstmt.executeQuery();
			
			while(rs.next()) {
				int id=rs.getInt("id");
				String name=rs.getString("name");
				String email=rs.getString("email");
				String phone=rs.getString("phone");
				LocalDate dt=LocalDate.parse(rs.getString("date_of_creation"));
				
				Customer c=new Customer(name, email, phone, dt);
				c.setCustomerId(id);
				
				customers.add(c);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
//		if(!customers.isEmpty()) {
//			return customers;
//		}
//		else {
//			return null;
//		}
		return customers.isEmpty()?null:customers;
	}
	
	

}
